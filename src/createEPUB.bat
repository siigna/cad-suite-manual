cd c:/siigna/siigna-manual

del siigna_manual.epub

C:\Progra~1\WinRAR\WinRAR.exe a -afzip -m0 -inul siigna_manual.zip mimetype
C:\Progra~1\WinRAR\WinRAR.exe a -afzip -m0 -inul siigna_manual.zip container.xml META-INF
C:\Progra~1\WinRAR\WinRAR.exe a -afzip -m0 -inul siigna_manual.zip *.xhtml EPUB
C:\Progra~1\WinRAR\WinRAR.exe a -afzip -m0 -inul siigna_manual.zip package.opf EPUB
C:\Progra~1\WinRAR\WinRAR.exe a -afzip -m0 -inul siigna_manual.zip epub.css EPUB\CSS
C:\Progra~1\WinRAR\WinRAR.exe a -afzip -m0 -inul siigna_manual.zip *.jpg EPUB\IMAGES

rename siigna_manual.zip siigna_manual.epub

java -jar epubcheck.jar siigna_manual.epub
pause